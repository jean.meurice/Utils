#ifndef H_UTILS_BITS_INCLUDE
#define H_UTILS_BITS_INCLUDE

#include "utypes.h"

namespace utils {

template<typename T>
inline T BIT(const u32 &pos) {
	return ((T)1 << (pos));
}

constexpr auto BITU8 = BIT<u8>;
constexpr auto BITU32 = BIT<u32>;
constexpr auto BITU64 = BIT<u64>;

template<typename T>
inline bool is_bit_high(const T &var, const u32 &pos) {
	return var & BIT<T>(pos);
}

template<typename T>
inline bool is_bit_low(const T &var, const u32 &pos) {
	return !is_bit_high(var, pos);
}

template<typename T>
inline void set_bit_high(T &var, const u32 &pos) {
	return var |= BIT<T>(pos);
}

template<typename T>
inline void set_bit_low(T &var, const u32 &pos) {
	return var &= ~BIT<T>(pos);
}

template<typename T, typename N>
inline void is_flag_high(const T &var, const T &flag) {
	return var & static_cast<T>(flag);
}

template<typename T, typename N>
inline void set_flag_high(T &var, const T &flag) {
	return var |= static_cast<T>(flag);
}

template<typename T, typename N>
inline void set_flag_low(T &var, const T &flag) {
	return var &= ~static_cast<T>(flag);
}

extern u32 BIT_MASKS[];


}

#endif