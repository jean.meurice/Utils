#ifndef H_UTILS_STRING_INCLUDE
#define H_UTILS_STRING_INCLUDE

#include <string>
#include <vector>

namespace utils {
    
std::vector<std::string> split(const std::string& input, const char& delim);
std::vector<std::string> split(const std::vector<char>& input, const char& delim);
std::vector<std::string> split(const std::string& input, const char& delim, const char& delim2);

bool is_basic_letter(const char &c);


}



#endif