#ifndef H_UTILS_TYPES_INCLUDE
#define H_UTILS_TYPES_INCLUDE

#include <cstdint>

namespace utils {

// using schar = int8_t;
// using sbyte = int8_t;
// using sshort = int16_t;
// using sint = int32_t;
// using slong = int64_t;

// using uchar = uint8_t;
// using ubyte = uint8_t;
// using ushort = uint16_t;
// using uint = uint32_t;
// using ulong = uint64_t;

// static_assert(sizeof(schar) == 1);
// static_assert(sizeof(uchar) == 1);
// static_assert(sizeof(sbyte) == 1);
// static_assert(sizeof(ubyte) == 1);
// static_assert(sizeof(sshort) == 2);
// static_assert(sizeof(ushort) == 2);
// static_assert(sizeof(sint) == 4);
// static_assert(sizeof(uint) == 4);
// static_assert(sizeof(slong) == 8);
// static_assert(sizeof(ulong) == 8);
// static_assert(sizeof(float) == 4);
// static_assert(sizeof(double) == 8);

using i8 = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;

using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

using usize = uintptr_t;

using f32 = float;
using f64 = double;


static_assert(sizeof(i8) == 1);
static_assert(sizeof(u8) == 1);
static_assert(sizeof(i16) == 2);
static_assert(sizeof(u16) == 2);
static_assert(sizeof(i32) == 4);
static_assert(sizeof(u32) == 4);
static_assert(sizeof(i64) == 8);
static_assert(sizeof(u64) == 8);
static_assert(sizeof(f32) == 4);
static_assert(sizeof(f64) == 8);

/*
    Vector of length 2 and type <T>.
    Members are accessed through x and y; min and max; data or operator[].
*/
template<typename T>
struct vec2 {
    union {
        struct {
            T x;
            T y;
        };
        T data[2];
        struct {
            T min;
            T max;
        };
    };
    constexpr vec2<T>( T x, T y ) : x( x ), y( y ) {}
    constexpr vec2<T>( T *data ) : x( data[0] ), y( data[1] ) {}
    constexpr vec2<T>() {}
    constexpr vec2<T>( T val ) : x( val ), y( val ) {}
    
    
    constexpr vec2<T> operator+( const vec2<T> &add ) const {
        return vec2<T>( x + add.x, y + add.y );
    }
    constexpr vec2<T> operator-( const vec2<T> &sub ) const {
        return vec2<T>( x - sub.x, y - sub.y );
    }
    
    //Component-wise multiplication
    constexpr vec2<T> operator*( const vec2<T> &mult ) const {
		return vec2<T>( x * mult.x, y * mult.y );
    }
    
    vec2<T> &operator += ( const vec2<T> &add ) {
        x += add.x;
        y += add.y;
        return *this;
    }
    vec2<T> &operator -= ( const vec2<T> &sub ) {
        x -= sub.x;
        y -= sub.y;
        return *this;
    }
    
    constexpr vec2<T> operator-() const {
        return vec2<T>( -x, -y );
    }
    
#define vec_scalar_op(OP) constexpr vec2<T> operator OP ( const T& val ) const { return vec2<T>(x OP val, y OP val); }
    vec_scalar_op( * )
    vec_scalar_op( / )
    vec_scalar_op( + )
    vec_scalar_op( - )
    vec_scalar_op( % )
#undef vec_scalar_op
    
#define vec_scalar_op(OP) vec2<T> &operator OP ( const T& val ) { x OP val; y OP val; return *this; }
    vec_scalar_op( *= )
    vec_scalar_op( /= )
    vec_scalar_op( += )
    vec_scalar_op( -= )
    vec_scalar_op( %= )
#undef vec_scalar_op
    
    //Cartesian length of the vector
    T operator!() const {
        return ( T ) std::sqrt( x * x + y * y );
    }
    
    //Cartesian length of the vector, also use !vec
    T length() const {
        return !*this;
    }
    
    //Returns normalized vector
    vec2 operator~() const {
        auto l = length();
        return l == 0 ? *this : *this / l;
    }
    
    //Returns a the normalized vector, also use ~vec.
    vec2 normalized() const {
        return ~*this;
    }
    
    inline T &operator[]( u32 i ) {
        //throw_assert( i < 2, "Vector data access param too big" );
        return data[i];
    }
    inline const T &operator[]( u32 i ) const {
        //throw_assert( i < 2, "Vector data access param too big" );
        return data[i];
    }
    
    template<typename A>
    explicit operator vec2<A>() const {
        return vec2<A>( ( A )x, ( A )y );
    }
};

/*
Vector of length 3 and type <T>.
Members are accessed through x, y and z; r g and b; data or operator[].
*/
template<typename T>
struct vec3 {
    union {
        struct {
            T x;
            T y;
            T z;
        };
        T data[3];
        struct {
            T r;
            T g;
            T b;
        };
    };
    vec3<T>( T x, T y, T z ) : x( x ), y( y ), z( z ) {}
    vec3<T>() {}
    vec3<T>( T val ) : x( val ), y( val ), z( val ) {}
    vec3<T>( T *data ) : x( data[0] ), y( data[1] ), z( data[2] ) {}
    
    vec3( const vec2<T> &v1, const T &val ) : x( v1.x ), y( v1.y ), z( val ) {}
    vec3( const T &val, const vec2<T> &v1 ) : x( val ), y( v1.x ), z( v1.y ) {}

    /*
     Swizzle operators
    */
    vec2<T> xy() const {
        return { x, y };
    }
    vec2<T> xz() const {
        return { x, z };
    }
    vec2<T> yz() const {
        return { y,z };
    }
    
    vec3<T> operator+( const vec3<T> &add ) const {
        return vec3<T>( x + add.x, y + add.y, z + add.z );
    }
    vec3<T> operator-( const vec3<T> &sub ) const {
        return vec3<T>( x - sub.x, y - sub.y, z - sub.z );
    }
    
    //Component-wise multiplication
    inline vec3<T> operator*( const vec3<T> &mult ) const {
		return vec3<T>( x * mult.x, y * mult.y, z * mult.z );
    }
    
    vec3<T> &operator += ( const vec3<T> &add ) {
        x += add.x;
        y += add.y;
        z += add.z;
        return *this;
    }
    vec3<T> &operator -= ( const vec3<T> &sub ) {
        x -= sub.x;
        y -= sub.y;
        z -= sub.z;
        return *this;
    }
    
    vec3<T> operator-() const {
        return vec3<T>( -x, -y, -z );
    }
    
#define vec_scalar_op(OP) vec3<T> operator OP ( const T& val ) const { return vec3<T>(x OP val, y OP val, z OP val); }
    vec_scalar_op( * )
    vec_scalar_op( / )
    vec_scalar_op( + )
    vec_scalar_op( - )
    vec_scalar_op( % )
#undef vec_scalar_op
    
#define vec_scalar_op(OP) vec3<T> &operator OP ( const T& val ) { x OP val; y OP val; z OP val; return *this; }
    vec_scalar_op( *= )
    vec_scalar_op( /= )
    vec_scalar_op( += )
    vec_scalar_op( -= )
    vec_scalar_op( %= )
#undef vec_scalar_op
    
    //Cartesian length of the vector
    T operator!() const {
        return ( T )sqrt( x * x + y * y + z * z );
    }
    
    //Cartesian length of the vector, also use !vec
    T length() const {
        return !*this;
    }
    inline T &operator[]( u32 i ) {
        //throw_assert( i < 3, "Vector data access param too big" );
        return data[i];
    }
    inline const T &operator[]( u32 i ) const {
        //throw_assert( i < 3, "Vector data access param too big" );
        return data[i];
    }
    
    //Returns normalized vector
    vec3 operator~() const {
        auto l = length();
        return l == 0 ? *this : *this / l;
    }
    
    //Returns a the normalized vector, also use ~vec.
    vec3 normalized() const {
        return ~*this;
    }
    
    /*
        Returns the cross product of two <vec3>, using right hand rule.
    */
    vec3<T> operator^(const vec3<T> &other ) const {
        return vec3<T>( y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x );
    }
    
    //Cross product: also use v1 ^ v2
    template<typename T>
    vec3<T> cross( const vec3<T> &v2 ) const {
        return *this ^ v2;
    }
    
    template<typename A>
    explicit operator vec3<A>() const {
        return vec3<A>( ( A )x, ( A )y, ( A )z );
    }
};

/*
    Vector of length 4 and type <T>.
    Members are accessed through x, y, z and u; r, g, b and a; data or operator[].
    Use length() to get the cartesian length of the vector.
    The Vertex addition and substration, as well as scalar multiplication and division are defined.
*/
template<typename T>
struct vec4 {
    union {
        struct {
            T x;
            T y;
            T z;
            T u;
        };
        T data[4];
        struct {
            T r;
            T g;
            T b;
            T a;
        };
    };
    
    inline vec4<T>( T x, T y, T z, T u ) : x( x ), y( y ), z( z ), u( u ) {}
    inline vec4<T>( T *data ) : x( data[0] ), y( data[1] ), z( data[2] ), u( data[3] ) {}
    inline vec4<T>() {}
    inline vec4<T>( T val ) : x( val ), y( val ), z( val ), u( val ) {}
    
    /*
     Swizzle operators
    */
    vec2<T> xy() const {
        return { x, y };
    }
    vec2<T> xz() const {
        return { x, z };
    }
    vec2<T> xu() const {
        return { x, u };
    }
    vec2<T> yz() const {
        return { y,z };
    }
    vec2<T> yu() const {
        return { y,u };
    }
    vec2<T> zu() const {
        return { z,u };
    }

    vec3<T> xyz() const {
        return { x,y,z };
    }
    vec3<T> xyu() const {
        return { x,y,u };
    }
    vec3<T> xzu() const {
        return { x,z,u };
    }
    vec3<T> yzu() const {
        return { y,z,u };
    }
    
    inline vec4<T> operator+( const vec4<T> &add ) const {
        return vec4<T>( x + add.x, y + add.y, z + add.z, u + add.u );
    }
    inline vec4<T> operator-( const vec4<T> &sub ) const {
        return vec4<T>( x - sub.x, y - sub.y, z - sub.z, u - sub.u );
    }
    
    //Component-wise multiplication
    inline vec4<T> operator*( const vec4<T> &mult ) const {
		return vec4<T>( x * mult.x, y * mult.y, z * mult.z, u * mult.u );
    }
    
    inline vec4<T> &operator += ( const vec4<T> &add ) {
        x += add.x;
        y += add.y;
        z += add.z;
        u += add.u;
        return *this;
    }
    inline vec4<T> &operator -= ( const vec4<T> &sub ) {
        x -= sub.x;
        y -= sub.y;
        z -= sub.z;
        u -= sub.u;
        return *this;
    }
    
    inline vec4<T> operator-() const {
        return vec4<T>( -x, -y, -z, -u );
    }
    
#define vec_scalar_op(OP) inline vec4<T> operator OP ( const T& val ) const { \
        return vec4<T>(x OP val, y OP val, z OP val, u OP val); \
    }
    vec_scalar_op( * )
    vec_scalar_op( / )
    vec_scalar_op( + )
    vec_scalar_op( - )
    vec_scalar_op( % )
#undef vec_scalar_op
    
#define vec_scalar_op(OP) inline vec4<T> &operator OP ( const T& val ) { x OP val; y OP val; z OP val; a OP val; return *this; }
    vec_scalar_op( *= )
    vec_scalar_op( /= )
    vec_scalar_op( += )
    vec_scalar_op( -= )
    vec_scalar_op( %= )
#undef vec_scalar_op
    
    //Returns the length of the vector
    inline T operator!() const {
        return ( T )sqrt( x * x + y * y + z * z + u * u );
    }
    
    //Cartesian length of the vector, also use !vec.
    inline T length() const {
        return !*this;
    }
    
    //Returns normalized vector
    inline vec4 operator~() const {
        auto l = length();
        return l == 0 ? *this : *this / l;
    }
    
    //Returns a the normalized vector, also use ~vec.
    inline vec4 normalized() const {
        return ~*this;
    }
    
    inline T &operator[]( u32 i ) {
        //throw_assert( i < 4, "Vector data access param too big" );
        return data[i];
    }
    inline const T &operator[]( u32 i ) const {
        //throw_assert( i < 4, "Vector data access param too big" );
        return data[i];
    }
    
    template<typename A>
    explicit operator vec4<A>() const {
        return vec4<A>( ( A )x, ( A )y, ( A )z, ( A ) a );
    }
};


/*
    Type shortcuts for common vector types.
*/
using  vec2i8 = vec2<i8>;
using  vec3i8 = vec3<i8>;
using  vec4i8 = vec4<i8>;

using  vec2u8 = vec2<u8>;
using  vec3u8 = vec3<u8>;
using  vec4u8 = vec4<u8>;

using  vec2i16 = vec2<i16>;
using  vec3i16 = vec3<i16>;
using  vec4i16 = vec4<i16>;

using  vec2u16 = vec2<u16>;
using  vec3u16 = vec3<u16>;
using  vec4u16 = vec4<u16>;

using  vec2i32 = vec2<i32>;
using  vec3i32 = vec3<i32>;
using  vec4i32 = vec4<i32>;

using  vec2u32 = vec2<u32>;
using  vec3u32 = vec3<u32>;
using  vec4u32 = vec4<u32>;

using  vec2i64 = vec2<i64>;
using  vec3i64 = vec3<i64>;
using  vec4i64 = vec4<i64>;

using  vec2u64 = vec2<u64>;
using  vec3u64 = vec3<u64>;
using  vec4u64 = vec4<u64>;

using  vec2f32 = vec2<f32>;
using  vec3f32 = vec3<f32>;
using  vec4f32 = vec4<f32>;

using  vec2f64 = vec2<f64>;
using  vec3f64 = vec3<f64>;
using  vec4f64 = vec4<f64>;

template<typename T>
struct mat2 {
	union {
        struct {
			vec2<T> col1;
			vec2<T> col2;
        };
        vec2<T> data[2];
    };

    mat2( const vec2<T> &col1, const vec2<T> &col2 ) : col1( col1 ), col2( col2 ) {}
    mat2() : col1( 0 ), col2( 0 ) {}
    mat2(
		const T m11, const T m12,
		const T m21, const T m22
	) : 
		col1( m11, m21 ), 
		col2( m12, m22 )
	{}

    
    inline mat2<T> operator+( const mat2<T> &add ) const {
        return mat2<T>( col1 + add.col1, col2 + add.col2 );
    }
    inline mat2<T> operator-( const mat2<T> &sub ) const {
        return mat2<T>( col1 - sub.col1, col2 - sub.col2 );
    }


	inline vec2<T> operator*(const vec2<T> &v) const {
		return vec2<T>(
			col1.x * v.x + col2.x * v.y,
			col1.y * v.x + col2.y * v.y
		);
	}
	inline mat2<T> operator*(const mat2<T> &m) const {
		return mat2<T>(*this * m.col1, *this * m.col2);
	}
    
	inline mat2<T> &operator *= ( const mat2<T> &m ) {
        col1 = *this * m.col1;
        col2 = *this * m.col2;
        return *this;
    }
    
    inline mat2<T> &operator += ( const mat2<T> &add ) {
        col1 += add.col1;
        col2 += add.col2;
        return *this;
    }
    inline mat2<T> &operator -= ( const mat2<T> &sub ) {
        col1 -= sub.col1;
        col2 -= sub.col2;
        return *this;
    }
    
    inline mat2<T> operator-() const {
        return mat2<T>( -col1, -col2 );
    }
    
#define vec_scalar_op(OP) inline mat2<T> operator OP ( const T& val ) const { return mat2<T>(col1 OP val, col2 OP val); }
    vec_scalar_op( * )
    vec_scalar_op( / )
    vec_scalar_op( + )
    vec_scalar_op( - )
    vec_scalar_op( % )
#undef vec_scalar_op
    
#define vec_scalar_op(OP) inline mat2<T> &operator OP ( const T& val ) { col1 OP val; col2 OP val; return *this; }
    vec_scalar_op( *= )
    vec_scalar_op( /= )
    vec_scalar_op( += )
    vec_scalar_op( -= )
    vec_scalar_op( %= )
#undef vec_scalar_op
    
	// Access Collum
    inline vec2<T> &operator[]( u32 i ) {
        //throw_assert( i < 2, "Mat2 data access param too big" );
        return data[i];
    }
	// Access Collum
    inline const vec2<T> &operator[]( u32 i ) const {
        //throw_assert( i < 3, "Mat2 data access param too big" );
        return data[i];
    }

    
    template<typename A>
    explicit operator mat2<A>() const {
        return mat2<A>( ( vec2<A> )col1, ( vec2<A> )col2 );
    }

	static mat2<T> unit(){
		return mat2<T>(
			vec2<T>(1,0),
			vec2<T>(0,1)
		);
	}
};


template<typename T>
struct mat3 {
	union {
        struct {
			vec3<T> col1;
			vec3<T> col2;
			vec3<T> col3;
        };
        vec3<T> data[3];
    };

    mat3( const vec3<T> &col1, const vec3<T> &col2, const vec3<T> &col3 ) : col1( col1 ), col2( col2 ), col3( col3 ) {}
    mat3(T a) : col1( a,a,a ), col2( a,a,a ), col3( a,a,a ) {}
	mat3() {}
    mat3(
		const T m11, const T m12, const T m13,
		const T m21, const T m22, const T m23,
		const T m31, const T m32, const T m33
	) : 
		col1( m11, m21, m31 ), 
		col2( m12, m22, m32 ), 
		col3( m13, m23, m33 )
	{}

    
    inline mat3<T> operator+( const mat3<T> &add ) const {
        return mat3<T>( col1 + add.col1, col2 + add.col2, col3 + add.col3 );
    }
    inline mat3<T> operator-( const mat3<T> &sub ) const {
        return mat3<T>( col1 - sub.col1, col2 - sub.col2, col3 - sub.col3 );
    }


	inline vec3<T> operator*(const vec3<T> &v) const {
		return vec3<T>(
			col1.x * v.x + col2.x * v.y + col3.x * v.z,
			col1.y * v.x + col2.y * v.y + col3.y * v.z,
			col1.z * v.x + col2.z * v.y + col3.z * v.z
		);
	}
	inline mat3<T> operator*(const mat3<T> &m) const {
		return mat3<T>(*this * m.col1, *this * m.col2, *this * m.col3);
	}
    
	inline mat3<T> &operator *= ( const mat3<T> &m ) {
        col1 = *this * m.col1;
        col2 = *this * m.col2;
        col3 = *this * m.col3;
        return *this;
    }
    
    inline mat3<T> &operator += ( const mat3<T> &add ) {
        col1 += add.col1;
        col2 += add.col2;
        col3 += add.col3;
        return *this;
    }
    inline mat3<T> &operator -= ( const mat3<T> &sub ) {
        col1 -= sub.col1;
        col2 -= sub.col2;
        col3 -= sub.col3;
        return *this;
    }
    
    inline mat3<T> operator-() const {
        return mat3<T>( -col1, -col2, -col3 );
    }
    
#define vec_scalar_op(OP) inline mat3<T> operator OP ( const T& val ) const { return mat3<T>(col1 OP val, col2 OP val, col3 OP val); }
    vec_scalar_op( * )
    vec_scalar_op( / )
    vec_scalar_op( + )
    vec_scalar_op( - )
    vec_scalar_op( % )
#undef vec_scalar_op
    
#define vec_scalar_op(OP) inline mat3<T> &operator OP ( const T& val ) { col1 OP val; col2 OP val; col3 OP val; return *this; }
    vec_scalar_op( *= )
    vec_scalar_op( /= )
    vec_scalar_op( += )
    vec_scalar_op( -= )
    vec_scalar_op( %= )
#undef vec_scalar_op
    
	// Access Collum
    inline vec3<T> &operator[]( u32 i ) {
        //throw_assert( i < 3, "Mat3 data access param too big" );
        return data[i];
    }
	// Access Collum
    inline const vec3<T> &operator[]( u32 i ) const {
        //throw_assert( i < 3, "Mat3 data access param too big" );
        return data[i];
    }

    
    template<typename A>
    explicit operator mat3<A>() const {
        return mat3<A>( ( vec3<A> )col1, ( vec3<A> )col2, ( vec3<A> )col3 );
    }

	static mat3<T> unit(){
		return mat3<T>(
			vec3<T>(1,0,0),
			vec3<T>(0,1,0),
			vec3<T>(0,0,1)
		);
	}
};

template<typename T>
struct mat4 {
	union {
        struct {
			vec4<T> col1;
			vec4<T> col2;
			vec4<T> col3;
			vec4<T> col4;
        };
        vec4<T> data[4];
    };

    mat4( const vec4<T> &col1, const vec4<T> &col2, const vec4<T> &col3, const vec4<T> &col4 ) : col1( col1 ), col2( col2 ), col3( col3 ), col4( col4 ) {}
    mat4() : col1( 0 ), col2( 0 ), col3( 0 ), col4( 0 )  {}
    mat4(
		const T m11, const T m12, const T m13, const T m14,
		const T m21, const T m22, const T m23, const T m24,
		const T m31, const T m32, const T m33, const T m34,
		const T m41, const T m42, const T m43, const T m44
	) : 
		col1( m11, m21, m31, m41 ), 
		col2( m12, m22, m32, m42 ), 
		col3( m13, m23, m33, m43 ), 
		col4( m14, m24, m34, m44 )  
	{}
    

    
    inline mat4<T> operator+( const mat4<T> &add ) const {
        return mat4<T>( col1 + add.col1, col2 + add.col2, col3 + add.col3, col4 + add.col4 );
    }
    inline mat4<T> operator-( const mat4<T> &sub ) const {
        return mat4<T>( col1 - sub.col1, col2 - sub.col2, col3 - sub.col3, col4 - sub.col4 );
    }


	inline vec4<T> operator*(const vec4<T> &v) const {
		return vec4<T>(
			col1.x * v.x + col2.x * v.y + col3.x * v.z + col4.x * v.u,
			col1.y * v.x + col2.y * v.y + col3.y * v.z + col4.y * v.u,
			col1.z * v.x + col2.z * v.y + col3.z * v.z + col4.z * v.u,
			col1.u * v.x + col2.u * v.y + col3.u * v.z + col4.u * v.u
		);
	}
	inline mat4<T> operator*(const mat4<T> &m) const {
		return mat4<T>(*this * m.col1, *this * m.col2, *this * m.col3, *this * m.col4);
	}
    
	inline mat4<T> &operator *= ( const mat4<T> &m ) {
        col1 = *this * m.col1;
        col2 = *this * m.col2;
        col3 = *this * m.col3;
        col4 = *this * m.col4;
        return *this;
    }
    
    inline mat4<T> &operator += ( const mat4<T> &add ) {
        col1 += add.col1;
        col2 += add.col2;
        col3 += add.col3;
        col4 += add.col4;
        return *this;
    }
    inline mat4<T> &operator -= ( const mat4<T> &sub ) {
        col1 -= sub.col1;
        col2 -= sub.col2;
        col3 -= sub.col3;
        col4 -= sub.col4;
        return *this;
    }
    
    inline mat4<T> operator-() const {
        return mat4<T>( -col1, -col2, -col3, -col4 );
    }
    
#define vec_scalar_op(OP) inline mat4<T> operator OP ( const T& val ) const { return mat4<T>(col1 OP val, col2 OP val, col3 OP val, col4 OP val); }
    vec_scalar_op( * )
    vec_scalar_op( / )
    vec_scalar_op( + )
    vec_scalar_op( - )
    vec_scalar_op( % )
#undef vec_scalar_op
    
#define vec_scalar_op(OP) inline mat4<T> &operator OP ( const T& val ) { col1 OP val; col2 OP val; col3 OP val; col4 OP val; return *this; }
    vec_scalar_op( *= )
    vec_scalar_op( /= )
    vec_scalar_op( += )
    vec_scalar_op( -= )
    vec_scalar_op( %= )
#undef vec_scalar_op
    
	// Access Collum
    inline vec4<T> &operator[]( u32 i ) {
        //throw_assert( i < 4, "Mat4 data access param too big" );
        return data[i];
    }
	// Access Collum
    inline const vec4<T> &operator[]( u32 i ) const {
        //throw_assert( i < 4, "Mat4 data access param too big" );
        return data[i];
    }

    
    template<typename A>
    explicit operator mat4<A>() const {
        return mat4<A>( ( vec4<A> )col1, ( vec4<A> )col2, ( vec4<A> )col3, ( vec4<A> )col4 );
    }

	static mat4<T> unit(){
		return mat4<T>(
			vec4<T>(1,0,0,0),
			vec4<T>(0,1,0,0),
			vec4<T>(0,0,1,0),
			vec4<T>(0,0,0,1)
		);
	}
};


/*
    Type shortcuts for common matrix types.
*/
using  mat2i32 = mat2<i32>;
using  mat3i32 = mat3<i32>;
using  mat4i32 = mat4<i32>;

using  mat2u32 = mat2<u32>;
using  mat3u32 = mat3<u32>;
using  mat4u32 = mat4<u32>;

using  mat2f32 = mat2<f32>;
using  mat3f32 = mat3<f32>;
using  mat4f32 = mat4<f32>;

using  mat2f64 = mat2<f64>;
using  mat3f64 = mat3<f64>;
using  mat4f64 = mat4<f64>;

using  mat2i16 = mat2<i16>;
using  mat3i16 = mat3<i16>;
using  mat4i16 = mat4<i16>;

using  mat2u16 = mat2<u16>;
using  mat3u16 = mat3<u16>;
using  mat4u16 = mat4<u16>;

using  mat2i8 = mat2<i8>;
using  mat3i8 = mat3<i8>;
using  mat4i8 = mat4<i8>;

using  mat2u8 = mat2<u8>;
using  mat3u8 = mat3<u8>;
using  mat4u8 = mat4<u8>;

using  mat2i64 = mat2<i64>;
using  mat3i64 = mat3<i64>;
using  mat4i64 = mat4<i64>;

using  mat2u64 = mat2<u64>;
using  mat3u64 = mat3<u64>;
using  mat4u64 = mat4<u64>;

}

#endif