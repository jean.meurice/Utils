#ifndef H_UTILS_MATH_INCLUDE
#define H_UTILS_MATH_INCLUDE

#include "utypes.h"
#include <limits>
#include <vector>
#include <string>
#include <cmath>

namespace utils {

template <class T>
inline void hash_combine(std::size_t& seed, const T& v)
{
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}


static constexpr float PIf = 3.14159265358979323846f;
static constexpr double PId = 3.14159265358979323846;

static constexpr float DEG_TO_RADf = 2.0f / 360.0f * PIf;
static constexpr double DEG_TO_RADd = 2.0 / 360.0 * PId;
static constexpr float RAD_TO_DEGf = 360.0f / ( 2.0f * PIf );
static constexpr double RAD_TO_DEGd = 360.0 / ( 2.0 * PId );

constexpr float INFf = std::numeric_limits<float>::infinity();
constexpr double INFd = std::numeric_limits<double>::infinity();


inline vec2f32 ufloor(const vec2f32& v) {
    return { std::floor(v.x), std::floor(v.y) };
}
inline vec3f32 ufloor(const vec3f32& v) {
    return {std::floor(v.x), std::floor(v.y), std::floor(v.z) };
}
inline vec4f32 ufloor(const vec4f32& v) {
    return { std::floor(v.x), std::floor(v.y), std::floor(v.z), std::floor(v.u) };
}
inline vec2f64 ufloor(const vec2f64& v) {
    return { std::floor(v.x), std::floor(v.y) };
}
inline vec3f64 ufloor(const vec3f64& v) {
    return { std::floor(v.x), std::floor(v.y), std::floor(v.z) };
}
inline vec4f64 ufloor(const vec4f64& v) {
    return { std::floor(v.x), std::floor(v.y), std::floor(v.z), std::floor(v.u) };
}

inline vec2f32 uceil(const vec2f32& v) {
    return { std::ceil(v.x), std::ceil(v.y) };
}
inline vec3f32 uceil(const vec3f32& v) {
    return { std::ceil(v.x), std::ceil(v.y), std::ceil(v.z) };
}
inline vec4f32 uceil(const vec4f32& v) {
    return { std::ceil(v.x), std::ceil(v.y), std::ceil(v.z), std::ceil(v.u) };
}
inline vec2f64 uceil(const vec2f64& v) {
    return { std::ceil(v.x), std::ceil(v.y) };
}
inline vec3f64 uceil(const vec3f64& v) {
    return { std::ceil(v.x), std::ceil(v.y), std::ceil(v.z) };
}
inline vec4f64 uceil(const vec4f64& v) {
    return { std::ceil(v.x), std::ceil(v.y), std::ceil(v.z), std::ceil(v.u) };
}

template<typename T>
inline i32 usgn( const T &val ) {
    return val > 0 ? 1 : val < 0 ? -1 : 0;
}

template<typename T>
inline T uabs( const T &a ) {
    return a >= 0 ? a : -a;
}

template<typename T>
inline T umin( const T &a, const T &b ) {
    return a < b ? a : b;
}
template<typename T>
inline T umax( const T &a, const T &b ) {
    return a > b ? a : b;
}

template<typename T>
inline T umin( const T &a, const T &b, const T &c ) {
    if ( a < b )
        return a < c ? a : c;
    else
        return b < c ? b : c;
}
template<typename T>
inline T umax( const T &a, const T &b, const T &c ) {
    if ( a > b )
        return a > c ? a : c;
    else
        return b > c ? b : c;
}

template<typename T>
inline void umaximize( T &target, const T &val ) {
    if ( val > target )
        target = val;
}
template<typename T>
inline void uminimize( T &target, const T &val ) {
    if ( val < target )
        target = val;
}

template<typename T>
inline bool uequals( const T &a, const T &b, const T &threshold ) {
	return a <= b + threshold && a >= b - threshold;
}



template<typename T, typename S>
inline T ulerp( const T &v1, const T &v2, const S &alpha ) {
    return ( v1 * ( 1 - alpha ) ) + ( v2 * alpha );
}



/*
Returns the cross product of two <vec3>, using right hand rule.
*/
template<typename T>
inline vec3<T> cross( const vec3<T> &v1, const vec3<T> &v2 ) {
    return vec3<T>( v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x );
}

/*
Returns the dot product of two vectors.
*/
template<typename T>
inline T dot( const vec4<T> &v1, const vec4<T> &v2 ) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.u * v2.u;
}

template<typename T>
inline T dot( const vec3<T> &v1, const vec3<T> &v2 ) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

template<typename T>
inline T dot( const vec2<T> &v1, const vec2<T> &v2 ) {
    return v1.x * v2.x + v1.y * v2.y;
}

/*
Returns the normalized <v> vector.
Returns the zero vector if given the zero vector.
*/
template<template<typename> typename U, typename T>
inline U<T> normalize( const U<T> &v ) {
    T l = v.length();
    
    if ( l == 0 )
        return v;
        
    return v / l;
}
/*
Returns the normalized <v> vector.
Returns the zero vector if given the zero vector.
*/
template<template<typename> typename U>
inline U<double> normalize( const U<double> &v ) {
    T l = v.length();
    
    if ( l < 0.00000001 && l > -0.00000001 )
        return v;
        
    return v / l;
}
/*
Returns the normalized <v> vector.
Returns the zero vector if given the zero vector.
*/
template<template<typename> typename U>
inline U<float> normalize( const U<float> &v ) {
    float l = v.length();
    
    if ( l < 0.000001f && l > -0.000001f )
        return v;
        
    return v / l;
}


template<typename T>
inline mat3<T> transpose(const mat3<T> &m){
	return mat3<T>(
		vec3<T>(m.col1.x, m.col2.x, m.col3.x),
		vec3<T>(m.col1.y, m.col2.y, m.col3.y),
		vec3<T>(m.col1.z, m.col2.z, m.col3.z)
	);
}

template<typename T>
inline mat4<T> extend_matrix(const mat3<T> &m){
	return mat4<T>(
		m.col1.x, m.col2.x, m.col3.x, 0,
		m.col1.y, m.col2.y, m.col3.y, 0,
		m.col1.z, m.col2.z, m.col3.z, 0,
		0,		  0,		0,		  1
	);
}

template<typename T>
inline mat4<T> translation_matrix(const vec3<T> &v){
	return mat4<T>(
		1,  0,  0,  v.x,
		0,  1,  0,  v.y,
		0,  0,  1,  v.z,
		0,  0,  0,  1
	);
}

template<typename T>
inline mat4<T> rotation_x_matrix(T angle_radians){
	return mat4<T>(
		1,  0,  0,  0,
		0,  cos(angle_radians),  -sin(angle_radians),  0,
		0,  sin(angle_radians),  cos(angle_radians),  0,
		0,  0,  0,  1
	);
}

template<typename T>
inline mat3<T> to_mat3(const mat4<T> &m){
	return mat3<T>(
		vec3<T>(m.col1.x, m.col1.y, m.col1.z),
		vec3<T>(m.col2.x, m.col2.y, m.col2.z),
		vec3<T>(m.col3.x, m.col3.y, m.col3.z)
	);
}


template<typename T>
inline mat4<T> diagonal_matrix(const vec4<T> &v){
	return mat4<T>(
		v.x, 0,   0,   0,
		0,   v.y, 0,   0,
		0,   0,   v.z, 0,
		0,   0,   0,   v.u
	);
}

template<typename T>
inline mat3<T> diagonal_matrix(const vec3<T> &v){
	return mat3<T>(
		v.x, 0,   0,
		0,   v.y, 0,
		0,   0,   v.z
	);
}

template<typename T>
inline mat3<T> cross_matrix(const vec3<T> &v){
	return mat3<T>(
		0,   -v.z,  v.y,
		v.z,  0,   -v.x,
	   -v.y,  v.x,  0
	);
}



template<typename T>
inline mat3<T> inverse(const mat3<T> &m, T &det){
	mat3<T> adj;
    adj.col1.x = m.col2.y*m.col3.z - m.col2.z*m.col3.y;
    adj.col2.x = m.col2.z*m.col3.x - m.col2.x*m.col3.z;
    adj.col3.x = m.col2.x*m.col3.y - m.col2.y*m.col3.x;

    det = m.col1.x * adj.col1.x + m.col1.y * adj.col2.x + m.col1.z * adj.col3.x;
    if (det < 0.0000001 && det > -0.0000001) { det = 0; }

    adj.col1.y = m.col3.y * m.col1.z - m.col3.z * m.col1.y;
    adj.col2.y = m.col3.z * m.col1.x - m.col3.x * m.col1.z;
    adj.col3.y = m.col3.x * m.col1.y - m.col3.y * m.col1.x;

    adj.col1.z = m.col1.y * m.col2.z - m.col1.z * m.col2.y;
    adj.col2.z = m.col1.z * m.col2.x - m.col1.x * m.col2.z;
    adj.col3.z = m.col1.x * m.col2.y - m.col1.y * m.col2.x;
    adj /= det;
    return adj;
}

template<typename T>
inline mat3<T> inverse(const mat3<T> &m){
	mat3<T> adj;
    adj.col1.x = m.col2.y*m.col3.z - m.col2.z*m.col3.y;
    adj.col2.x = m.col2.z*m.col3.x - m.col2.x*m.col3.z;
    adj.col3.x = m.col2.x*m.col3.y - m.col2.y*m.col3.x;

    T det = m.col1.x * adj.col1.x + m.col1.y * adj.col2.x + m.col1.z * adj.col3.x;
    if (det < 0.0000001 && det > -0.0000001) { det = 0; }

    adj.col1.y = m.col3.y * m.col1.z - m.col3.z * m.col1.y;
    adj.col2.y = m.col3.z * m.col1.x - m.col3.x * m.col1.z;
    adj.col3.y = m.col3.x * m.col1.y - m.col3.y * m.col1.x;

    adj.col1.z = m.col1.y * m.col2.z - m.col1.z * m.col2.y;
    adj.col2.z = m.col1.z * m.col2.x - m.col1.x * m.col2.z;
    adj.col3.z = m.col1.x * m.col2.y - m.col1.y * m.col2.x;
    adj /= det;
    return adj;
}

template<typename T>
inline void orthonormize(mat3<T> &m){
	vec3<T> r23, r31, r12;
	m.col1 = normalize(m.col1);
	m.col2 = normalize(m.col2);
	m.col3 = normalize(m.col3);
	while (
		!uequals<T>(dot(m.col1, m.col2), 0, 0.0001) || 
		!uequals<T>(dot(m.col1, m.col3), 0, 0.0001) ||
		!uequals<T>(dot(m.col3, m.col2), 0, 0.0001)
	) {
		r23 = cross(m.col2, m.col3);
		r31 = cross(m.col3, m.col1);
		r12 = cross(m.col1, m.col2);
		m.col1 = normalize(m.col1+r23);
		m.col2 = normalize(m.col2+r31);
		m.col3 = normalize(m.col3+r12);
	}
}
template<typename T>
inline void orthonormize2(mat3<T> &m){
	m.col2 -= m.col1 * dot(m.col1, m.col2);
	m.col3 -= m.col1 * dot(m.col1, m.col3);
	m.col3 -= m.col2 * dot(m.col2, m.col3);
	
	m.col1 = normalize(m.col1);
	m.col2 = normalize(m.col2);
	m.col3 = normalize(m.col3);

}

template<typename T>
class range {
		struct range_iterator {
			T val;
			range_iterator( T val ) : val( val ) {}
			range_iterator() : val( 0 ) {}
			range_iterator operator++() {
				return ++val;
			}
			T operator*() {
				return val;
			}
			bool operator !=( const range_iterator &other ) {
				return other.val != val;
			}
		};
    public:
    
        range( T start, T end ) : m_start( start ), m_end( end ) {}
        range() : m_start( 0 ), m_end( 0 ) {}
        range( T size ) : m_start( 0 ), m_end( size ) {}
        range_iterator begin() {
            return m_start;
        }
        range_iterator end() {
            return m_end;
        }
    private:
    
        T m_start;
        T m_end;
};

using rangei8 = range<i8>;
using rangeu8 = range<u8>;
using rangei16 = range<i16>;
using rangeu16 = range<u16>;
using rangei32 = range<i32>;
using rangeu32 = range<u32>;
using rangei64 = range<i64>;
using rangeu64 = range<u64>;
using rangef32 = range<f32>;
using rangef64 = range<f64>;


template<typename T, typename U>
void ufill(std::vector<T> &v, const U &val){
    for (auto &i : v){
        i = val;
    }
}

class UAverageCounter {
    u64 amount;
    u64 total;
    double average;
    u64 minv;
    u64 maxv;
public:
    UAverageCounter() : amount(0), total(0), average(0), minv(UINT64_MAX), maxv(0) {}
    void add(u64 val) {
        ++amount;
        total += val;
        if (val > maxv) maxv = val;
        if (val < minv) minv = val;
    }
    double get() {
        if (amount != 0) average = (double)total / amount;
        return average;
    }
    u64 get_min() { return minv; }
    u64 get_max() { return maxv; }
    std::string get_desc() {
        return "avg:" + std::to_string(get()) + " min:" + std::to_string(minv) + " max:" + std::to_string(maxv);
    }
    /*void print() {
        std::cout << "avg:" << get() << " min:" << minv << " max:" << maxv;
    }*/
};

}

#endif