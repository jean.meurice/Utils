#pragma once
#include <chrono>
#include <string>
#include "utypes.h"

namespace utils {

struct PrecisionTimer {
    using HRClock = std::chrono::high_resolution_clock;
    using TimePoint = HRClock::time_point;
    static inline u64 time_delta(TimePoint start, TimePoint end) {
        return std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
    }
    static std::string microsecond_time_to_string(u64 time) {
        if (time >= 1000000)
            return std::to_string(time / 1000000) + "." + std::to_string((time / 1000) % 1000) + "s";
        else if (time >= 1000)
            return std::to_string(time / 1000) + "." + std::to_string(time % 1000) + "ms";
        else
            return std::to_string(time) + "us";
    }
    TimePoint start_time, end_time;

    std::string name;
    inline void start() {
        start_time = HRClock::now();
    }
    inline void end() {
        end_time = HRClock::now();
    }
    //Returns timer delta in microseconds
    inline long get_delta() {
        return (long)time_delta(start_time, end_time);
    }
    std::string print(std::string tab = std::string()) {
        return microsecond_time_to_string(get_delta());
    }
};

}