#pragma once
#include "utils/utypes.h"
#include <vector>
#include <iostream>
#include <string>
#include <array>
#include <vector>
#include <list>
#include <map>
#include <unordered_map>
#include <set>

using namespace utils;

struct Buffer {
	Buffer& write(const char* s, std::streamsize n);
	Buffer& read(char* s, std::streamsize n);
	u64 size() const {
		return data.size();
	}
private:
	std::vector<char> data;
	u64 read_pos = 0;
};

namespace Serial {

	extern const u64 byte_boundary[9];
	//Variable Length Unsigned Int Encoding
	template<typename I, u8 MAX_BYTES>
	struct VLU {
		I value;
		VLU() {}
		VLU(const I& v) : value(v) {}

		template<typename B>
		void write(B& target) const {
			u8 i = 0;
			for (; i < MAX_BYTES; ++i) {
				if ((I)value < byte_boundary[i]) break;
			}
			for (u8 j = i; j > 0; --j) {
				u8 b = static_cast<u8>(((value >> (7 * j)) & 0b01111111) | 0b10000000);
				target.write(reinterpret_cast<const char*>(&b), 1);
			}
			u8 b = static_cast<u8>(value & 0b01111111);
			target.write(reinterpret_cast<const char*>(&b), 1);
		}

		template<typename B>
		void read(B& target) {
			value = 0;
			u8 b;
			target.read(reinterpret_cast<char*>(&b), 1);
			while (b & 0b10000000) {
				value |= static_cast<I>(b & 0b01111111);
				value <<= 7;
				target.read(reinterpret_cast<char*>(&b), 1);
			}
			value |= static_cast<I>(b & 0b01111111);
		}

		bool operator==(const VLU& other) const {
			return value == other.value;
		}
	};

	using VLu32 = VLU<u32, 4>;
	using VLu64 = VLU<u64, 9>;

	
	template<typename B>
	struct Serializer {
		Serializer(B &target) : target(target) {}

		void write_short(const u16* s) {
			u8 b = static_cast<u8>(*s >> 8);
			target.write(reinterpret_cast<const char*>(&b), 1);
			b = static_cast<u8>(*s);
			target.write(reinterpret_cast<const char*>(&b), 1);
		}

		void write_int(const u32* s) {
			u8 b = static_cast<u8>(*s >> 24);
			target.write(reinterpret_cast<const char*>(&b), 1);
			b = static_cast<u8>(*s >> 16);
			target.write(reinterpret_cast<const char*>(&b), 1);
			b = static_cast<u8>(*s >> 8);
			target.write(reinterpret_cast<const char*>(&b), 1);
			b = static_cast<u8>(*s);
			target.write(reinterpret_cast<const char*>(&b), 1);
		}

		void write_long(const u64* s) {
			u8 b = static_cast<u8>(*s >> 56);
			target.write(reinterpret_cast<const char*>(&b), 1);
			b = static_cast<u8>(*s >> 48);
			target.write(reinterpret_cast<const char*>(&b), 1);
			b = static_cast<u8>(*s >> 40);
			target.write(reinterpret_cast<const char*>(&b), 1);
			b = static_cast<u8>(*s >> 32);
			target.write(reinterpret_cast<const char*>(&b), 1);
			b = static_cast<u8>(*s >> 24);
			target.write(reinterpret_cast<const char*>(&b), 1);
			b = static_cast<u8>(*s >> 16);
			target.write(reinterpret_cast<const char*>(&b), 1);
			b = static_cast<u8>(*s >> 8);
			target.write(reinterpret_cast<const char*>(&b), 1);
			b = static_cast<u8>(*s);
			target.write(reinterpret_cast<const char*>(&b), 1);
		}

		void serialize(const u8& i) {
			target.write(reinterpret_cast<const char*>(&i), 1);
		}
		void serialize(const i8& i) {
			target.write(reinterpret_cast<const char*>(&i), 1);
		}
		void serialize(const u16& i) {
			write_short(&i);
		}
		void serialize(const i16& i) {
			write_short(reinterpret_cast<const u16*>(&i));
		}
		void serialize(const u32& i) {
			write_int(&i);
		}
		void serialize(const i32& i) {
			write_int(reinterpret_cast<const u32*>(&i));
		}
		void serialize(const u64& i) {
			write_long(&i);
		}
		void serialize(const i64& i) {
			write_long(reinterpret_cast<const u64*>(&i));
		}
		void serialize(const float& i) {
			write_int(reinterpret_cast<const u32*>(&i));
		}
		void serialize(const double& i) {
			write_long(reinterpret_cast<const u64*>(&i));
		}


		void serialize(const VLu32& v) {
			v.write(target);
		}
		void serialize(const VLu64& v) {
			v.write(target);
		}

		void serialize(const std::string& s) {
			VLu64 size(s.size());
			serialize(size);
			for (const auto& c : s) {
				i8 b = c;
				serialize(b);
			}
		}

		void serialize(const std::wstring& s) {
			VLu64 size(s.size());
			serialize(size);
			for (const auto& c : s) {
				i16 b = c;
				serialize(b);
			}
		}

		template<typename T>
		void serialize(const std::vector<T>& s) {
			VLu64 size(s.size());
			serialize(size);
			for (const auto& c : s) {
				serialize(c);
			}
		}

		template<typename T, size_t N>
		void serialize(const std::array<T, N>& a) {
			for (const auto& i : a) {
				serialize(i);
			}
		}

		template<typename T>
		void serialize(const std::list<T>& s) {
			VLu64 size(s.size());
			serialize(size);
			for (const auto& c : s) {
				serialize(c);
			}
		}

		template<typename T>
		void serialize(const std::set<T>& s) {
			VLu64 size(s.size());
			serialize(size);
			for (const auto& c : s) {
				serialize(c);
			}
		}

		template<typename T, typename U>
		void serialize(const std::map<T, U>& m) {
			VLu64 size(s.size());
			serialize(size);
			for (const auto& c : m) {
				serialize(c.first);
				serialize(c.second);
			}
		}


		/*template<typename T>
		void serialize(const T& object) {
			T.traverse<Serializer>(*this);
		}*/

		template<typename T>
		void propose(const T& first) {
			serialize(first);
		}
		template<typename T, typename... Args>
		void propose(const T& first, const Args&... args) {
			serialize(first);
			propose(args...);
		}

	private:
		B& target;
	};

	template<typename B>
	struct Deserializer {
		Deserializer(B& target) : target(target) {}

		void read_short(u16* s) {
			u8 b;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s = static_cast<u64>(b) << 8;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s |= static_cast<u64>(b);
		}

		void read_int(u32* s) {
			u8 b;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s = static_cast<u64>(b) << 24;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s |= static_cast<u64>(b) << 16;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s |= static_cast<u64>(b) << 8;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s |= static_cast<u64>(b);
		}

		void read_long(u64* s) {
			u8 b;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s = static_cast<u64>(b) << 56;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s |= static_cast<u64>(b) << 48;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s |= static_cast<u64>(b) << 40;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s |= static_cast<u64>(b) << 32;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s |= static_cast<u64>(b) << 24;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s |= static_cast<u64>(b) << 16;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s |= static_cast<u64>(b) << 8;
			target.read(reinterpret_cast<char*>(&b), 1);
			*s |= static_cast<u64>(b);
		}

		u8 deserialize(u8& i) {
			target.read(reinterpret_cast<char*>(&i), sizeof(u8));
			return i;
		}
		i8 deserialize(i8& i) {
			target.read(reinterpret_cast<char*>(&i), sizeof(i8));
			return i;
		}
		u16 deserialize(u16& i) {
			read_short(&i);
			return i;
		}
		i16 deserialize(i16& i) {
			read_short(reinterpret_cast<u16*>(&i));
			return i;
		}
		u32 deserialize(u32& i) {
			read_int(&i);
			return i;
		}
		i32 deserialize(i32& i) {
			read_int(reinterpret_cast<u32*>(&i));
			return i;
		}
		u64 deserialize(u64& i) {
			read_long(&i);
			return i;
		}
		i64 deserialize(i64& i) {
			read_long(reinterpret_cast<u64*>(&i));
			return i;
		}

		float deserialize(float& i) {
			read_int(reinterpret_cast<u32*>(&i));
			return i;
		}
		double deserialize(double& i) {
			read_long(reinterpret_cast<u64*>(&i));
			return i;
		}

		void deserialize(VLu32& v) {
			v.read(target);
		}
		void deserialize(VLu64& v) {
			v.read(target);
		}

		void deserialize(std::string& s) {
			VLu64 size;
			deserialize(size);
			s.resize(size.value);
			for (auto& c : s) {
				i8 b;
				deserialize(b);
				c = b;
			}
		}
		void deserialize(std::wstring& s) {
			VLu64 size;
			deserialize(size);
			s.resize(size.value);
			for (auto& c : s) {
				i16 b;
				deserialize(b);
				c = b;
			}
		}

		template<typename T, size_t N>
		void deserialize(std::array<T, N>& a) {
			for (auto& i : a) {
				deserialize(i);
			}
		}

		template<typename T>
		void deserialize(std::vector<T>& s) {
			VLu64 size;
			deserialize(size);
			s.resize(size.value);
			for (auto& c : s) {
				deserialize(b);
			}
		}

		template<typename T>
		void deserialize(std::list<T>& s) {
			s.clear();
			VLu64 size;
			deserialize(size);
			for (auto i : range(size.value)) {
				s.push_back(T());
				auto& last = *s.rend();
				deserialize(last);
			}
		}

		template<typename T>
		void deserialize(std::set<T>& s) {
			s.clear();
			VLu64 size;
			deserialize(size);
			for (auto i : range(size.value)) {
				auto& last = *s.insert(T()).first;
				deserialize(last);
			}
		}

		/*template<typename T>
		void deserialize(T& object) {
			object.traverse<Deserializer>(*this);
		}*/
		template<typename T>
		void propose(T& first) {
			deserialize(first);
		}
		template<typename T, typename... Args>
		void propose(T& first, Args& ... args) {
			deserialize(first);
			propose(args...);
		}






		

		VLu32&& deserialize(VLu32&& v) {
			v.read(target);
			return v;
		}
		VLu64&& deserialize(VLu64&& v) {
			v.read(target);
			return v;
		}

		std::string&& deserialize(std::string&& s) {
			VLu64 size;
			deserialize(size);
			s.resize(size.value);
			for (auto& c : s) {
				i8 b;
				c = deserialize(b);
			}
			return s;
		}
		std::wstring&& deserialize(std::wstring&& s) {
			VLu64 size;
			deserialize(size);
			s.resize(size.value);
			for (auto& c : s) {
				i16 b;
				deserialize(b);
				c = b;
			}
			return s;
		}

		template<typename T, size_t N>
		std::array<T, N>&& deserialize(std::array<T, N>&& a) {
			for (auto& i : a) {
				deserialize(i);
			}
			return a;
		}

		template<typename T>
		std::vector<T>&& deserialize(std::vector<T>&& s) {
			VLu64 size;
			deserialize(size);
			s.resize(size.value);
			for (auto& c : s) {
				deserialize(b);
			}
			return s;
		}

		template<typename T>
		std::list<T>&& deserialize(std::list<T>&& s) {
			s.clear();
			VLu64 size;
			deserialize(size);
			for (auto i : range(size.value)) {
				s.push_back(deserialize(T()));
			}
			return s;
		}

		template<typename T>
		std::set<T>&& deserialize(std::set<T>&& s) {
			s.clear();
			VLu64 size;
			deserialize(size);
			for (auto i : range(size.value)) {
				s.insert(deserialize(T()));
			}
			return s;
		}


	private:
		B& target;
	};

}