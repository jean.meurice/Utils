#pragma once

#include <exception>
#include <string>

/*

Add somewhere in the test project:

#include "tests.h"

int main() {
	if (test_component("Component A", component_a_tests)) return -1;
	if (test_component("Component B", component_b_tests)) return -2;
	...
	return 0;
}

// Example for a "Component test":
void component_a_tests(bool& flag) {
	test("CompA::Foo test", foo_test, flag);
	test("CompA::Bar test", bar_test, flag);
}

*/

namespace Assert {
	class assert_exception : public std::exception {
		std::string description;
	public:
		assert_exception(const std::string& description) : description("Assert exception: " + description) {}
		virtual const char* what() const throw()
		{
			return description.c_str();
		}
	};

	inline void True(bool exp, const std::string& error) {
		if (!exp) throw assert_exception(error);
	}
	inline void False(bool exp, const std::string& error) {
		if (exp) throw assert_exception(error);
	}
	template<typename T>
	inline void Equal(const T& t1, const T& t2, const std::string& error) {
		if (!(t1 == t2)) throw assert_exception(error);
	}
}


//Sets flag to true or returns true when an error occured

namespace utils {
		
	void test( const char *name, bool( *func )(), bool &flag);
	bool test_component( const char *name, void( *func )(bool& flag));
	
}
