#ifndef H_UTILS_STREAM_INCLUDE
#define H_UTILS_STREAM_INCLUDE


#include <iostream>
#include "utils/uconsole.h"
#include "utils/utypes.h"


namespace Log {
    struct TagStruct {};
    extern TagStruct tag;
    class LogStream {
            utils::ConsoleColor color;
            const char *tag;
        public:
            bool hide;
            LogStream( utils::ConsoleColor color, const char *tag ) : color( color ), tag( tag ), hide( false ) {}
            LogStream &operator<<( const TagStruct &param ) {
                if ( hide )
                    return *this;
                utils::uconsole.set_color( color );
                printf( "%-7s", tag );
                return *this;
            }
            template<typename T>
            LogStream &operator<<( const T &param ) {
                if ( hide )
                    return *this;
                utils::uconsole.set_color( color );
                std::cout << param;
                return *this;
            }
    };
    
    extern LogStream info;
    extern LogStream err;
    extern LogStream debug;
    extern LogStream note;
    extern LogStream assert;
    extern LogStream test;
    extern LogStream test_success;
	extern LogStream test_error;
}







//template<typename T> struct utils::vec2;
template<typename T>
std::ostream &operator<<( std::ostream &out, const utils::vec2<T> &vec ) {
    out << "[" << vec.x << ", " << vec.y << "]";
    return out;
}

//template<typename T> struct vec3;
template<typename T>
std::ostream &operator<<( std::ostream &out, const utils::vec3<T> &vec ) {
    out << "[" << vec.x << ", " << vec.y << ", " << vec.z << "]";
    return out;
}

//template<typename T> struct vec4;
template<typename T>
std::ostream &operator<<( std::ostream &out, const utils::vec4<T> &vec ) {
    out << "[" << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.a << "]";
    return out;
}

//template<typename T> struct mat4;
template<typename T>
std::ostream &operator<<( std::ostream &out, const utils::mat4<T> &m ) {
    out << "\n  | " << m.col1.x << " " << m.col2.x << " " << m.col3.x << " " << m.col4.x << " |";
    out << "\n  | " << m.col1.y << " " << m.col2.y << " " << m.col3.y << " " << m.col4.y << " |";
    out << "\n  | " << m.col1.z << " " << m.col2.z << " " << m.col3.z << " " << m.col4.z << " |";
    out << "\n  | " << m.col1.u << " " << m.col2.u << " " << m.col3.u << " " << m.col4.u << " |\n";
    return out;
}

//template<typename T> struct mat3;
template<typename T>
std::ostream &operator<<( std::ostream &out, const utils::mat3<T> &m ) {
    out << "\n  | " << m.col1.x << " " << m.col2.x << " " << m.col3.x << " |";
    out << "\n  | " << m.col1.y << " " << m.col2.y << " " << m.col3.y << " |";
    out << "\n  | " << m.col1.z << " " << m.col2.z << " " << m.col3.z << " |\n";
    return out;
}

//template<typename T> struct mat2;
template<typename T>
std::ostream &operator<<( std::ostream &out, const utils::mat2<T> &m ) {
    out << "\n  | " << m.col1.x << " " << m.col2.x << " |";
    out << "\n  | " << m.col1.y << " " << m.col2.y << " |\n";
    return out;
}


#endif