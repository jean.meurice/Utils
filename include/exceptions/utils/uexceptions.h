#ifndef H_UTILS_EXCEPTIONS_INCLUDE
#define H_UTILS_EXCEPTIONS_INCLUDE

#include <string>
#include <sstream>
#include <iostream>

/*
    DEBUG MODE ASSERTIONS
*/

struct AssertionFailureException : public std::exception {
    AssertionFailureException( const char *expression, const char *file, int line,
                               const char *message ) {
        std::ostringstream outputStream;
        
        std::string message_string( message );
        if ( !message_string.empty() )
            outputStream << message_string << ": ";
            
        std::string expressionString = expression;
        if ( expressionString == "false" || expressionString == "0" || expressionString == "FALSE" )
            outputStream << "Unreachable code assertion";
        else
            outputStream << "Assertion '" << expression << "'";
            
        outputStream << " failed in file '" << file << "' line " << line;
        std::cerr << outputStream.str() << std::endl;
    }
};

// Assert that EXPRESSION evaluates to true, otherwise raise AssertionFailureException with associated MESSAGE
#ifdef _DEBUG
    #define throw_assert(EXPRESSION, MESSAGE) if(!(EXPRESSION)) { throw AssertionFailureException(#EXPRESSION, __FILE__, __LINE__,  MESSAGE); }
#else
    #define throw_assert(E, M) ((void)0)
#endif

/*
    Exception Type for System specific errors.
    lasterr() will read the details of the last system error (GetLastError() under Windows) and return an exception that
    can be thrown.
*/
class SystemException : public std::exception {
    std::string description;
public:
    SystemException(const std::string& description) : description("SystemError: " + description) {}
    SystemException(const std::string& description, const char* file, int line) : description(
        "C++ Simulator Error at " + std::string(file) + "[line: " + std::to_string(line) + "]: " + description
    ) {}
    static SystemException lasterr(const std::string& description, const char* file, int line);
    static SystemException lasterr();
    virtual const char* what() const throw()
    {
        return description.c_str();
    }
};

/*
    These macros record the file and line at which they are called into the exception description.
*/
#define throw_system_error(MESSAGE) throw SystemException(MESSAGE, __FILE__, __LINE__)
#define throw_lasterr(MESSAGE) throw SystemException::lasterr(MESSAGE, __FILE__, __LINE__)


#endif