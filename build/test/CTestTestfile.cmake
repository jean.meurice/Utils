# CMake generated Testfile for 
# Source directory: D:/dev/Utils/test
# Build directory: D:/dev/Utils/build/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
if("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
  add_test(UtilsTest "D:/dev/Utils/build/test/Debug/utils_test.exe")
  set_tests_properties(UtilsTest PROPERTIES  WORKING_DIRECTORY "D:/dev/Utils/test/bin" _BACKTRACE_TRIPLES "D:/dev/Utils/test/CMakeLists.txt;19;add_test;D:/dev/Utils/test/CMakeLists.txt;0;")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
  add_test(UtilsTest "D:/dev/Utils/build/test/Release/utils_test.exe")
  set_tests_properties(UtilsTest PROPERTIES  WORKING_DIRECTORY "D:/dev/Utils/test/bin" _BACKTRACE_TRIPLES "D:/dev/Utils/test/CMakeLists.txt;19;add_test;D:/dev/Utils/test/CMakeLists.txt;0;")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
  add_test(UtilsTest "D:/dev/Utils/build/test/MinSizeRel/utils_test.exe")
  set_tests_properties(UtilsTest PROPERTIES  WORKING_DIRECTORY "D:/dev/Utils/test/bin" _BACKTRACE_TRIPLES "D:/dev/Utils/test/CMakeLists.txt;19;add_test;D:/dev/Utils/test/CMakeLists.txt;0;")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
  add_test(UtilsTest "D:/dev/Utils/build/test/RelWithDebInfo/utils_test.exe")
  set_tests_properties(UtilsTest PROPERTIES  WORKING_DIRECTORY "D:/dev/Utils/test/bin" _BACKTRACE_TRIPLES "D:/dev/Utils/test/CMakeLists.txt;19;add_test;D:/dev/Utils/test/CMakeLists.txt;0;")
else()
  add_test(UtilsTest NOT_AVAILABLE)
endif()
