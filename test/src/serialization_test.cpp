#include <utils/userialization.h>
#include <utils/utest.h>


bool test_buffer_basic_types() {
	u8 a0 = 200;
	u8 b0 = 0;
	i8 c0 = 100;
	i8 d0 = -12;
	i16 e0 = -1200;
	u16 f0 = 1200;
	i32 g0 = -512546;
	u32 h0 = 556542;
	i64 i0 = -231456878945L;
	u64 j0 = 231456878945L;
	float k0 = 0.0f;
	float l0 = -5646.6568f;
	double m0 = 84684684684.654654654654;

	Buffer buff;
	Serial::Serializer serializer(buff);
	serializer.propose(a0, b0, c0, d0, e0, f0, g0, h0, i0, j0, k0, l0, m0);
	
	u64 expected_size = sizeof(a0) + sizeof(b0) + sizeof(c0) + sizeof(d0)
		+ sizeof(e0) + sizeof(f0) + sizeof(g0) + sizeof(h0)
		+ sizeof(i0) + sizeof(j0) + sizeof(k0) + sizeof(l0) + sizeof(m0);
	Assert::Equal<u64>(buff.size(), expected_size, "Buffer doesn't contain expected size of data.");

	u8 a1;
	u8 b1;
	i8 c1;
	i8 d1;
	i16 e1;
	u16 f1;
	i32 g1;
	u32 h1;
	i64 i1;
	u64 j1;
	float k1;
	float l1;
	double m1;

	Serial::Deserializer deserializer(buff);
	deserializer.propose(a1, b1, c1, d1, e1, f1, g1, h1, i1, j1, k1, l1, m1);

	Assert::Equal(a0, a1, "u8 a0 != a1");
	Assert::Equal(b0, b1, "u8 b0 != b1");
	Assert::Equal(c0, c1, "i8 c0 != c1");
	Assert::Equal(d0, d1, "i8 d0 != d1");
	Assert::Equal(e0, e1, "i16 e0 != e1");
	Assert::Equal(f0, f1, "u16 f0 != f1");
	Assert::Equal(g0, g1, "i32 g0 != g1");
	Assert::Equal(h0, h1, "u32 h0 != h1");
	Assert::Equal(i0, i1, "i64 i0 != i1");
	Assert::Equal(j0, j1, "u64 j0 != j1");
	Assert::Equal(k0, k1, "float k0 != k1");
	Assert::Equal(l0, l1, "float l0 != l1");
	Assert::Equal(m0, m1, "double m0 != m1");

	return false;
}

bool test_buffer_VLU() {
	Serial::VLu32 a0(51);
	Serial::VLu32 b0(5178);
	Serial::VLu32 c0(124456);
	Serial::VLu32 d0(200113468);
	Serial::VLu64 e0(45123789456);
	Serial::VLu64 f0(44512370894516);
	Serial::VLu64 g0(4451123752370894516);
	Serial::VLu64 h0(0);

	Buffer buff;
	Serial::Serializer serializer(buff);
	serializer.propose(a0, b0, c0, d0, e0, f0, g0, h0);

	u64 expected_size = 1 + 2 + 3 + 4 + 6 + 7 + 9 + 1;
	Assert::Equal<u64>(buff.size(), expected_size, "Buffer doesn't contain expected size of data.");

	Serial::VLu32 a1;
	Serial::VLu32 b1;
	Serial::VLu32 c1;
	Serial::VLu32 d1;
	Serial::VLu64 e1;
	Serial::VLu64 f1;
	Serial::VLu64 g1;
	Serial::VLu64 h1;

	Serial::Deserializer deserializer(buff);
	deserializer.propose(a1, b1, c1, d1, e1, f1, g1, h1);

	Assert::Equal(a0, a1, "VLu32 a0 != a1");
	Assert::Equal(b0, b1, "VLu32 b0 != b1");
	Assert::Equal(c0, c1, "VLu32 c0 != c1");
	Assert::Equal(d0, d1, "VLu32 d0 != d1");
	Assert::Equal(e0, e1, "VLu64 e0 != e1");
	Assert::Equal(f0, f1, "VLu64 f0 != f1");
	Assert::Equal(g0, g1, "VLu64 g0 != g1");
	Assert::Equal(h0, h1, "VLu64 h0 != h1");

	return false;
}


bool test_buffer_string() {
	std::string a0("");
	std::string b0("random_string");
	std::string c0("special_chars\t\n\0aze");
	std::wstring d0(L"wstring string");
	std::wstring e0(L"");
	

	Buffer buff;
	Serial::Serializer serializer(buff);
	serializer.propose(a0, b0, c0, d0, e0);

	u64 expected_size = a0.size() + 1 + b0.size() + 1 + c0.size() + 1 + d0.size()*2 + 1 + e0.size()*2 + 1;
	Assert::Equal<u64>(buff.size(), expected_size, "Buffer doesn't contain expected size of data.");

	std::string a1;
	std::string b1;
	std::string c1;
	std::wstring d1;
	std::wstring e1;

	Serial::Deserializer deserializer(buff);
	deserializer.propose(a1, b1, c1, d1, e1);

	Assert::Equal(a0, a1, "std::string a0 != a1");
	Assert::Equal(b0, b1, "std::string b0 != b1");
	Assert::Equal(c0, c1, "std::string c0 != c1");
	Assert::Equal(d0, d1, "std::wstring d0 != d1");
	Assert::Equal(e0, e1, "std::wstring e0 != e1");

	return false;
}

bool test_buffer_array() {
	std::array<u8, 3> a0 = {1,5,200};
	//string
	//float
	//i8
	//i32
	//i64

	Buffer buff;
	Serial::Serializer serializer(buff);
	serializer.propose(a0);

	std::array<u8, 3> a1;

	Serial::Deserializer deserializer(buff);
	deserializer.propose(a1);

	Assert::Equal(a0, a1, "std::array<u8, 3> a0 != a1");

	return true;
}


bool test_buffer_vector() {
	//copy array cases

	return true;
}


void serialization_tests(bool& flag) {
	test("Serial:: basic types buffer", test_buffer_basic_types, flag);
	test("Serial:: Variable Length Unsigned Integers", test_buffer_VLU, flag);
	test("Serial:: std::string", test_buffer_string, flag);
	test("Serial:: std::array", test_buffer_array, flag);
	test("Serial:: std::vector", test_buffer_vector, flag);
}