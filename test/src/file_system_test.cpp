#include <utils/ustream.h>
#include <utils/ufilesystem.h>
#include <utils/utest.h>
#include "test_list.h"

using namespace utils;


namespace FS {
	struct FileSystemTests {
		static void construct_directory(const std::string& path, const std::string& expected_path, const std::string& name) {
			FS::Directory d(path);
			Assert::Equal<std::string>(d.get_path(), expected_path,
				path + " wrongly processed: " + d.get_path() + " (expected: "+ expected_path +")");
			Assert::Equal<std::string>(d.get_name(), name,
				path + " wrong get_name(): " + d.get_name() + " (expected: " + name + ")");
		}

		static bool directory_construction() {
			construct_directory("", "", "");
			construct_directory("relative/path", "relative/path", "path");
			construct_directory("./temp/test//../", "./temp/test/..", "..");
			construct_directory("/absolute_path/.//dir", "/absolute_path/./dir", "dir");
			construct_directory("C:/users/test/", "C:/users/test", "test");
			construct_directory("./temp\\test\\/../", "./temp/test/..", "..");
			
			bool ok = false;
			try {
				FS::Directory d("C:relative/path");
			}
			catch (const std::exception& e) {
				ok = true;
			}
			Assert::True(ok, "Did not throw an error when using a relative path with drive letter specifier.");
			return false;
		}

		static bool directory_append() {
			auto d1 = FS::Directory("C:/users/test/") + FS::Directory ("relative/path");
			Assert::Equal<std::string>(d1.get_path(), "C:/users/test/relative/path",
				"\"C:/users/test/\" + \"relative/path\"" + std::string(" wrongly processed: ") + d1.get_path());

			auto d2 = FS::Directory("./rel/path\\to") + FS::Directory("../relative/path");
			Assert::Equal<std::string>(d2.get_path(), "./rel/path/to/../relative/path",
				"\"./rel/path\\to\" + \"../relative/path\"" + std::string(" wrongly processed: ") + d2.get_path());

			auto d4 = FS::Directory("./rel/path\\to") + FS::Directory("");
			Assert::Equal<std::string>(d4.get_path(), "./rel/path/to",
				"\"./rel/path\\to\" + \"\"" + std::string(" wrongly processed: ") + d4.get_path());

			auto d5 = FS::Directory("") + FS::Directory("../relative/path");
			Assert::Equal<std::string>(d5.get_path(), "../relative/path",
				"\"\" + \"../relative/path\"" + std::string(" wrongly processed: ") + d5.get_path());

			bool ok = false;
			try {
				auto d3 = FS::Directory("./rel/path\\to") + FS::Directory("C:/absolute/path/");
			}
			catch (const std::exception& e) {
				ok = true;
			}
			Assert::True(ok, "Did not throw an error when appending an absolute path to another path.");
			return false;
		}

		static bool directory_absolute() {
			Assert::True(FS::Directory("C:/users/test/").is_absolute(), "C:/users/test/ not considered absolute.");
			Assert::True(FS::Directory("/absolute_path/.//dir").is_absolute(), "/absolute_path/.//dir not considered absolute.");
			Assert::True(FS::Directory("~/absolute_path/.//dir").is_absolute(), "~/absolute_path/.//dir not considered absolute.");
			Assert::False(FS::Directory("./rel/path").is_absolute(), "./rel/path considered absolute.");
			Assert::False(FS::Directory("../rel/path").is_absolute(), "../rel/path considered absolute.");
			Assert::False(FS::Directory("rel/path").is_absolute(), "rel/path considered absolute.");
			return false;
		}

		static void test_file(const FS::File& file, const std::string& input,
			const std::string& full_name,
			const std::string& name,
			const std::string& ext)
		{
			Assert::Equal<std::string>(file.get_full_name(), full_name,
				"File " + input + " wrong full_name(): " + file.get_full_name());
			Assert::Equal<std::string>(file.get_name(), name,
				"File " + input + " wrong name(): " + file.get_name());
			Assert::Equal<std::string>(file.get_extension(), ext,
				"File " + input + " wrong extension(): " + file.get_extension());
		}

		static void test_file_folder(const FS::File& file, const std::string& input,
			const std::string& full_name,
			const std::string& name,
			const std::string& ext,
			const std::string& folder)
		{
			test_file(file, input, full_name, name, ext);
			Assert::Equal<std::string>(file.folder.get_path(), folder,
				"File " + input + " wrong folder path(): " + file.folder.get_path());
		}


		static bool file_constructor() {
			//Test without directory (since Directory constructor & directory append are tested).
			// => File(name)
			test_file(FS::File("file_name"), "file_name", "file_name", "file_name", "");
			test_file(FS::File("file_name.ext"), "file_name.ext", "file_name.ext", "file_name", ".ext");
			test_file(FS::File("file_name."), "file_name.", "file_name.", "file_name", ".");
			test_file(FS::File(".gitignore"), ".gitignore", ".gitignore", "", ".gitignore");

			test_file_folder(FS::File("rel/path/file_name"), "rel/path/file_name",
				"file_name", "file_name", "", "rel/path");
			test_file_folder(FS::File("C:/abs/path/to/file_name"), "C:/abs/path/to/file_name",
				"file_name", "file_name", "", "C:/abs/path/to");
			test_file_folder(FS::File("rel/path\\file_name.ext"), "rel/path\\file_name.ext",
				"file_name.ext", "file_name", ".ext", "rel/path");
			test_file_folder(FS::File("rel/path/.gitignore"), "rel/path/.gitignore",
				".gitignore", "", ".gitignore", "rel/path");
			test_file_folder(FS::File("rel/path/file_name"), "rel/path/file_name",
				"file_name", "file_name", "", "rel/path");

			bool ok = false;
			try {
				FS::File f1("");
			}
			catch (const std::exception& e) {
				ok = true;
			}
			Assert::True(ok, "Did not throw an error when creating a File with empty name.");

			ok = false;
			try {
				FS::File f2("rel/path/");
			}
			catch (const std::exception& e) {
				ok = true;
			}
			Assert::True(ok, "Did not throw an error when creating a File with only a path (and empty name).");
			return false;
		}


		static bool directory_content_test() {
			/*
				Contents of "filesystemtest" directory should be:
					dir
					file1.txt
					file2.txt
			*/
			Log::debug << Log::tag << "Current Directory: " << FS::current_directory().get_path() << "\n";
			auto test_dir = Directory("filesystemtest");
			auto contents = test_dir.get_contents();
			bool contains_dir = false;
			bool contains_file1 = false;
			bool contains_file2 = false;
			for (auto& d : contents.directories) {
				if (d.get_name() == "dir") contains_dir = true;
				else throw Assert::assert_exception("Unexpected directory in filesystemtest: " + d.get_name());
			}
			for (auto& f : contents.files) {
				if (f.get_full_name() == "file1.txt") contains_file1 = true;
				else if (f.get_full_name() == "file2.txt") contains_file2 = true;
				else throw Assert::assert_exception("Unexpected file in filesystemtest: " + f.get_full_name());
			}
			return !(contains_dir && contains_file1 && contains_file2);
		}

		static void test_canonical(const std::string& input, const std::string& expected) {
			auto d = Directory(input).canonical();
			Assert::Equal<std::string>(d.get_path(), expected,
				"Directory " + input + " wrong canonical(): " + d.get_path() + " (expected: " + expected + ")");
		}

		static bool canonical() {
			test_canonical("A:/user/../folder/./dir", "A:/folder/dir");
			return false;
		}
	};
}







void file_system_tests(bool& flag) {
	test("FS::Directory constructor", FS::FileSystemTests::directory_construction, flag);
	test("FS::Directory operator +", FS::FileSystemTests::directory_append, flag);
	test("FS::Directory is_absolute()", FS::FileSystemTests::directory_absolute, flag);
	test("FS::File constructor", FS::FileSystemTests::file_constructor, flag);
	test("FS::Directory get_contents()", FS::FileSystemTests::directory_content_test, flag);
	test("FS::Directory canonical()", FS::FileSystemTests::canonical, flag);
}