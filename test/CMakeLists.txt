cmake_minimum_required(VERSION 3.15)


list(APPEND TEST_SOURCEFILES
    src/test_main.cpp
    src/test_list.h
    src/file_system_test.cpp
    src/serialization_test.cpp
    src/test_bits.cpp
)

add_executable(utils_test ${TEST_SOURCEFILES})
target_link_libraries(utils_test PUBLIC utils utest)

set_target_properties(utils_test PROPERTIES 
VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/test/bin"
)

add_test( NAME UtilsTest COMMAND utils_test WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/test/bin")