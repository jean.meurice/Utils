#include "utils/ustream.h"


using namespace utils;

Log::TagStruct Log::tag;
Log::LogStream Log::info( ConsoleColor::DEFAULT, "[I]" );
Log::LogStream Log::err( ConsoleColor::RED, "[ERR]" );
Log::LogStream Log::debug( ConsoleColor::GREEN, "[DBG]" );
Log::LogStream Log::note( ConsoleColor::WHITE, "[NOTE]" );
Log::LogStream Log::assert( ConsoleColor::PINK, "[ASSERT]" );
Log::LogStream Log::test(ConsoleColor::PINK, "[TEST]");
Log::LogStream Log::test_success(ConsoleColor::GREEN, "[TEST]");
Log::LogStream Log::test_error(ConsoleColor::RED, "[TEST]");

