#include "utils/utest.h"
#include "utils/ustream.h"
#include <exception>

namespace utils {

	void test( const char *name, bool( *func )(), bool &flag){
		//Log::test << Log::tag << "Testing " << name << "\n";
		try {
			if (func()) {
				Log::test_error << Log::tag << "Test \"" << name << "\" failed\n";
				flag = true;
				return;
			}
		}
		catch (const std::exception& e) {
			Log::test_error << Log::tag << "Exception in test \"" << name << "\":\n\t" << e.what() << "\n";
			flag = true;
			return;
		}
		Log::test_success << Log::tag << "Test \"" << name << "\" succeeded\n";
	}

	bool test_component(const char* name, void(*func)(bool& flag)) {
		Log::test << Log::tag << "Testing component " << name << "\n";
		try {
			bool flag = false;
			func(flag);
			if (flag) {
				Log::test_error << Log::tag << "Testing component " << name << " failed\n";
				return true;
			}
		}
		catch (const std::exception& e) {
			Log::test_error << Log::tag << "Exception when testing component " << name << ":\n\t" << e.what() << "\n";
			return true;
		}
		Log::test_success << Log::tag << "Testing component " << name << " succeeded\n";
		return false;
	}

}