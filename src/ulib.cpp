#include "utils/umath.h"
#include "utils/utypes.h"
#include "utils/ustring.h"

#include <vector>
#include <string>





namespace utils {
    
    u32 BIT_MASKS[] = {
        0b0,
        0b1,                0b11,               0b111,              0b1111,
        0b11111,            0b111111,           0b1111111,          0b11111111,
        0b111111111,        0b1111111111,       0b11111111111,      0b111111111111,
        0b1111111111111,    0b11111111111111,   0b111111111111111,  0b1111111111111111
    };

	std::vector<std::string> split(const std::string& input, const char& delim, const char& delim2) {
		std::vector<std::string> res;
		size_t pos = 0;
		for (auto i : range(input.size())) {
			if (input[i] == delim || input[i] == delim2) {
				res.emplace_back(input.substr(pos, i - pos));
				pos = i + 1;
			}
		}
		res.emplace_back(input.substr(pos, input.size() - pos));
		return res;
	}

    std::vector<std::string> split(const std::string& input, const char& delim) {
        std::vector<std::string> res;
		size_t pos = 0;
        for (auto i : range(input.size())) {
            if (input[i] == delim) {
                res.emplace_back(input.substr(pos, i-pos));
                pos = i+1;
            }
        }
        res.emplace_back(input.substr(pos, input.size() - pos));
        return res;
    }


    std::vector<std::string> split(const std::vector<char>& input, const char& delim) {
        std::vector<std::string> res;
        size_t pos = 0;
        for (auto i : range(input.size())) {
            if (input[i] == delim) {
                res.emplace_back(input.data() + pos, i - pos);
                pos = i + 1;
            }
        }
        res.emplace_back(input.data() + pos, input.size() - pos);
        return res;
    }

    bool is_basic_letter(const char &c){
        //65 (A) -> 90 (Z), 97 (a) -> 122 (z)
        return (c >= 65 && c <= 90) || (c >= 97 && c <= 122);
    }
}
