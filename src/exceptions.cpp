#include "utils/uexceptions.h"

#if defined _WIN32 || defined _WIN64
#include <windows.h>

namespace utils {
    std::string get_last_error_str() {
        TCHAR buff[1024];
        DWORD dw = GetLastError();

        FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL,
            dw, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            buff, 1024, NULL);
        return std::string(buff);
    }
}

#else

namespace utils {
    std::string get_last_error_str() {
        auto msg = strerror(errno);
        return std::string(msg);
    }
}

#endif


SystemException SystemException::lasterr(const std::string& description, const char* file, int line)
{
    return SystemException(description + ("\n\tSystem error: " + utils::get_last_error_str()), file, line);
}

SystemException SystemException::lasterr()
{
    return SystemException("SystemException: " + utils::get_last_error_str());
}
