#include "utils/userialization.h"
#include "utils/umath.h"

Buffer& Buffer::write(const char* s, std::streamsize n)
{
	if (n == 1) {
		data.emplace_back(*s);
		return *this;
	}
	data.reserve(data.size() + n);
	for (auto i : range(n)) {
		data.emplace_back(s[i]);
	}
	return *this;
}

Buffer& Buffer::read(char* s, std::streamsize n)
{
	if (read_pos + n <= data.size()) {
		for (auto i : range(n)) {
			s[i] = data[read_pos++];
		}
		return *this;
	}
	else throw std::exception("Read too much from Buffer.");
}


namespace Serial {
	const u64 byte_boundary[9] = {
		(1LL << 7),  //128
		(1LL << 14), //16384
		(1LL << 21), //2097152
		(1LL << 28), //268435456
		(1LL << 35), //34359738368
		(1LL << 42), //4398046511104
		(1LL << 49), //562949953421312
		(1LL << 56), //72057594037927936
		(1LL << 63)  //9223372036854775807
	};
}