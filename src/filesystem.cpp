#include "utils/ufilesystem.h"
#include "utils/uexceptions.h"
#include "utils/ustring.h"


using namespace utils;


namespace file_reader {
    bool read(const fs::path& path, std::vector<utils::u8>& target, bool include_eof) {
        auto file = std::ifstream(path, std::ios::in | std::ios::binary | std::ios::ate);
        if (!file.is_open()) return false;
        auto pos = file.tellg();
        utils::u32 size = (utils::u32)pos;
        target.resize(size + (include_eof ? 1LL : 0));
        file.seekg(0, std::ios::beg);
        file.read((char*)target.data(), size);
        file.close();
        if (include_eof) target[size] = '\0';
        return true;
    }
    bool read(const fs::path& path, std::vector<char>& target, bool include_eof) {
        auto file = std::ifstream(path, std::ios::in | std::ios::binary | std::ios::ate);
        if (!file.is_open()) return false;
        auto pos = file.tellg();
        utils::u32 size = (utils::u32)pos;
        target.resize(size + (include_eof ? 1LL : 0));
        file.seekg(0, std::ios::beg);
        file.read(target.data(), size);
        file.close();
        if (include_eof) target[size] = '\0';
        return true;
    }
    std::string read_if_exists(const fs::path& path, bool include_eof) {
        std::ifstream file = std::ifstream(path, std::ios::in | std::ios::binary | std::ios::ate);
        if (!file.is_open()) //throw_system_error("Could not open file: " + file_path.to_string());
            return std::string();
        auto size = (utils::usize) file.tellg();
        std::string res(size + (include_eof ? 1LL : 0), '\0');
        file.seekg(0, std::ios::beg);
        file.read(res.data(), size);
        if (include_eof) res[size] = '\0';
        return res;
    }
}