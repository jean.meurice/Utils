#include "utils/uconsole.h"
#include "utils/umath.h"
#include <iostream>
#include <iomanip>

namespace utils {
    Console uconsole;
}


#if defined _WIN32 || defined _WIN64
#include <Windows.h>

namespace utils {
    Console::Console() {
        hstdout = GetStdHandle( STD_OUTPUT_HANDLE );
        GetConsoleScreenBufferInfo( hstdout, ( PCONSOLE_SCREEN_BUFFER_INFO )&csbi );
        
        current_color.text_color = ( ConsoleColor::ColorValue ) - 1;
        current_color.bg_color = ( ConsoleColor::ColorValue ) - 1;
        set_color( ConsoleColor::DEFAULT );
    }

    Console::~Console() {
        SetConsoleTextAttribute( hstdout, ( ( PCONSOLE_SCREEN_BUFFER_INFO )&csbi )->wAttributes );
    }

    void Console::set_color( ConsoleColor which ) {
        if ( current_color != which ) {
            current_color = which;
            SetConsoleTextAttribute( hstdout, which.get() );
        }
    }

    void Console::test_color() {
        for ( auto i : rangei32( 16 ) ) {
            std::cout << '\t';
            for ( auto j : rangei32( 16 ) ) {
                auto v = i + j * 16;
                set_color( ConsoleColor( ( ConsoleColor::ColorValue )i, ( ConsoleColor::ColorValue )j ) );
                std::cout << " ";
                std::cout << std::setw( 3 ) << v;
                std::cout << " ";
                set_color( ConsoleColor::DEFAULT );
                std::cout << " ";
            }
            std::cout << std::endl;
        }
    }
}

#else

namespace utils {
    Console::Console() {
        current_color.text_color = 0;
        current_color.bg_color = 0;
    }

    Console::~Console() {
        set_color( ConsoleColor::DEFAULT );
    }


    void Console::set_color( ConsoleColor which ) {
        if ( which.text_color != current_color.text_color ) {
            if ( which.bg_color != current_color.bg_color )
                std::cout << "\033[" << which.text_color << ";" << ( which.bg_color + 10 ) << "m";
            else
                std::cout << "\033[" << which.text_color << "m";
        }
        else if ( which.bg_color != current_color.bg_color )
            std::cout << "\033[" << ( which.bg_color + 10 ) << "m";
        current_color = which;
    }

    void Console::test_color() {
        for ( auto u : irange( 2 ) ) {
            sint ubase = u == 0 ? 30 : 90;
            for ( auto i : irange( 8 ) ) {
                std::cout << '\t';
                for ( auto v : irange( 2 ) ) {
                    sint vbase = v == 0 ? 30 : 90;
                    for ( auto j : irange( 8 ) ) {
                        set_color( ConsoleColor( ( ConsoleColor::ColorValue )( ubase + i ), ( ConsoleColor::ColorValue )( vbase + j ) ) );
                        std::cout << " ";
                        std::cout << std::setw( 3 ) << v;
                        std::cout << " ";
                        set_color( ConsoleColor::DEFAULT );
                        std::cout << " ";
                    }
                }
                std::cout << std::endl;
            }
        }
        
    }
}


#endif