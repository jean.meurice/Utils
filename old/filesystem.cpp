#include "utils/ufilesystem.h"
#include "utils/uexceptions.h"
#include "utils/ustring.h"


using namespace utils;


namespace FS {

    void fill_directories_files(const Directory& directory, std::vector<Directory>* directories, std::vector<File>* files);

    Directory::Directory(const std::string& p) {
        this->path = "";
        if (p.size() == 0) return;
        auto parts = utils::split(p, '\\', '/');
        bool first = true;
        for (auto& e : parts) {
            if (e.size() > 0) {
                this->path += first ? e : "/" + e;
                first = false;
            }
            else if (first) this->path += '/'; //Preserve absolute path
        }
        if (this->path.size() >= 3 && this->path[1] == ':' && this->path[2] != '/')
            throw SystemException("Using drive letter with relative path: " + this->path);
    }




    Directory operator+(const Directory& folder, const Directory& folder2)
    {
        if (folder2.is_absolute() && folder.get_path().size() > 0)
            throw SystemException("Called operator+(Directory(" + folder.get_path() + "), Directory(" + folder2.get_path() + ")): " + folder2.get_path() + " must be a relative directory.");
        if (folder.path.size() == 0) return folder2;
        if (folder2.path.size() == 0) return folder;
        Directory d;
        d.path = folder.path + "/" + folder2.path;
        return d;
    }

    bool Directory::is_absolute() const
    {
        return
            (path.size() >= 2 && path[1] == ':' && utils::is_basic_letter(path[0])) ||
            (path.size() > 0 && (path[0] == '/' || path[0] == '~'));
    }

    DirectoryContent Directory::get_contents() const
    {
        DirectoryContent c;
        fill_directories_files(*this, &c.directories, &c.files);
        return c;
    }

    std::vector<File> Directory::get_files() const
    {
        std::vector<File> f;
        fill_directories_files(*this, nullptr, &f);
        return f;
    }

    std::vector<Directory> Directory::get_directories() const
    {
        std::vector<Directory> d;
        fill_directories_files(*this, &d, nullptr);
        return d;
    }

    std::string Directory::get_name() const
    {
        auto i = path.size();
        while (i > 0 && path[i - 1] != '/') --i;
        return path.substr(i, std::string::npos);
    }

    std::string Directory::to_string() const
    {
        return path;
    }

    File::File(const std::string& folder, const std::string& name) : File(Directory(folder), name) {}

    File::File(const std::string& name) : File(Directory(), name) {}



    File::File(const Directory& folder, const std::string& name) : folder(folder) {
        i32 i = (i32)name.size() - 1;
        while (i >= 0 && name[i] != '/' && name[i] != '\\') --i;
        if (i >= 0) {
            this->folder = folder + Directory(name.substr(0, i));
            this->name = name.substr(i + 1LL, std::string::npos);
        }
        else {
            this->folder = folder;
            this->name = name;
        }
        if (this->name.size() == 0) throw SystemException("Creating File with empty name.");
        i = (i32)this->name.size() - 1;
        while (i >= 0 && this->name[i] != '.') --i;
        this->ext_pos = i >= 0 ? (u32)i : (u32)this->name.size();
    }

    File operator+(const Directory& folder, const File& file)
    {
        File f;
        f.folder = folder + file.folder;
        f.name = file.name;
        f.ext_pos = file.ext_pos;
        return f;
    }

    std::string File::to_string() const
    {
        if (folder.get_path().size() > 0)
            return folder.to_string() + "/" + name;
        return name;
    }



#if defined _WIN32 || defined _WIN64
#include <windows.h>
#include <shlwapi.h>


    std::string File::as_system_path() const
    {
        if (folder.get_path().size() > 0)
            return folder.as_system_path() + name;
        else return name;
    }


    Directory current_directory()
    {
        TCHAR buff[MAX_PATH];
        if (GetCurrentDirectory(sizeof(buff), buff) != 0)
            return Directory(buff);
        throw_lasterr("GetCurrentDirectory");
    }

    Directory Directory::canonical() const
    {
        /*if (!is_absolute())
            throw_system_error("Directory.canonical() only supported on absolute paths. (Called on "+to_string()+")");*/
        TCHAR buff[MAX_PATH];
        if (PathCanonicalizeA(buff, as_system_path().c_str())) {
            return Directory(buff);
        }
        throw_lasterr("PathCanonicalizeA");
    }

    bool Directory::exists() const
    {
        throw SystemException("Not implemented");
    }

    bool Directory::mkdir() const
    {
        throw SystemException("Not implemented");
    }



    std::string Directory::as_system_path() const
    {
        std::string p = path;
        for (auto& c : p) {
            if (c == '/') c = '\\';
        }
        return p.size() > 0 ? p + '\\' : "";
    }

    bool File::exists() const
    {
        return PathFileExistsA(as_system_path().c_str());
    }






    void fill_directories_files(const Directory& directory, std::vector<Directory>* directories, std::vector<File>* files) {
        WIN32_FIND_DATA fdFile;
        HANDLE hFind = NULL;

        //std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
        //std::wstring wide = converter.from_bytes(folder);
        if ((hFind = FindFirstFile((directory.as_system_path() + "*").c_str(), &fdFile)) == INVALID_HANDLE_VALUE)
            throw_lasterr("fill_directories_files()");

        do {
            std::string file_name = (const char*)fdFile.cFileName;
            if (file_name != "." && file_name != "..") {
                if (fdFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                    if (directories) directories->emplace_back(Directory(file_name));
                }
                else {
                    if (files) files->emplace_back(File(file_name));
                }
            }
        } while (FindNextFile(hFind, &fdFile));

        FindClose(hFind);
    }



#else

    std::string File::as_system_path() const
    {
        if (folder.get_path().size() > 0)
            return folder.as_system_path() + '/' + name;
        else return name;
    }

    Directory current_directory()
    {
        auto cwd = get_current_dir_name();
        if (cwd != NULL) {
            Directory d(cwd);
            free(cwd);
            return d;
        }
        throw_lasterr("get_current_dir_name()");
    }

    Directory Directory::canonical() const
    {
        //if (!is_absolute())
        //    throw_system_error("Directory.canonical() only supported on absolute paths.");

        char actualpath[PATH_MAX];
        if (realpath(path.c_str(), actualpath) != NULL)
            return Directory(actualpath);
        throw_lasterr("realpath()");
    }

    bool Directory::exists() const
    {
        throw SystemException("Not implemented");
    }

    bool Directory::mkdir() const
    {
        throw SystemException("Not implemented");
    }



    std::string Directory::as_system_path() const
    {
        return path;
    }

    bool File::exists() const
    {
        throw SystemException("Not implemented");
    }






    void fill_directories_files(const Directory& directory, std::vector<Directory>* directories, std::vector<File>* files) {
        DIR* d;
        struct dirent* dir;
        d = opendir(directory.as_system_path().c_str());
        if (!d)
            throw_lasterr("opendir()");
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG) {
                if (files) files->emplace_back(File(directory, dir->d_name));
            }
            else if (dir->d_type == DT_DIR) {
                if (directories) directories->emplace_back(directory + Directory(dir->d_name));
            }
        }
        closedir(d);
    }

#endif

}
